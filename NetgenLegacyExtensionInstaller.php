<?php

namespace Netgen\Composer;

use Composer\Package\PackageInterface;
use eZ\Publish\Composer\LegacyExtensionInstaller;

class NetgenLegacyExtensionInstaller extends LegacyExtensionInstaller
{
    public function supports($packageType)
    {
        return $packageType === 'netgen-legacy-extension';
    }

    public function getInstallPath(PackageInterface $package)
    {
        $extra = $this->composer->getPackage()->getExtra();
        if (!isset($extra['netgen-legacy-install'])) {
            return parent::getInstallPath($package);
        }

        list($vendor, $extensionName) = explode('/', $package->getPrettyName(), 2);

        $this->initializeVendorDir();

        return $this->vendorDir . '/' . $extensionName;
    }
}
