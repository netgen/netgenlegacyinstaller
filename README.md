# Composer installer for eZ Publish Legacy Stack (Netgen Variant)

This installer lets you install extensions for eZ Publish legacy (4.x) with [Composer](http://getcomposer.org).

## Installable extensions
To be able to install a legacy extension, it must be properly exposed to Composer with a valid composer.json file
(check [Composer documentation](http://getcomposer.org/doc/) for more information), declaring an `netgen-legacy-extension` type.

Example for eZ Tags:

```json
{
    "name": "netgen/eztags",
    "type": "netgen-legacy-extension",
    "require": {
        "netgen/netgen-legacy-installer": "*"
    }
}
```

### Extension name vs package name
The legacy extension gets installed in a directory named by your package pretty name. For example, package `netgen/eztags` gets installed in a directory named `eztags`. Currently, this is not configurable.

## How to install in my project
All you need to do is create a composer.json at the root of your project and require the extension (if the extension is not published on packagist, you also need to tell composer where to find it):

```json
{
    "name": "myvendorname/myproject",
    "description": "My super cool eZ Publish project",
    "license": "GPL-2.0",
    "minimum-stability": "dev",
    "require": {
        "php": ">=5.3.3",
        "netgen/eztags": "~2.0"
    },
    "repositories" : [
        {
             "type": "vcs",
             "url": "https://bitbucket.org/netgen/eztags.git"
        }
    ]
}
```

Then run `php composer.phar install` (assuming you have already properly installed Composer of course :wink:).

### eZ Publish 5 case
By default, the legacy extension installer assumes that eZ Publish legacy is installed in the current folder; in other words, it is configured for pure-eZ Publish 4 projects. If this is not the case (like in eZ Publish 5, where it resides in the ezpublish_legacy/ folder), then you'll need to configure where it is:

```json
{
    "name": "myvendorname/myproject",
    "description": "My super cool eZ Publish 5 project",
    "license": "GPL-2.0",
    "minimum-stability": "dev",
    "require": {
        "php": ">=5.3.3",
        "netgen/eztags": "~2.0"
    },
    "repositories" : [
        {
             "type": "vcs",
             "url": "https://bitbucket.org/netgen/eztags.git"
        }
    ],
    "extra": {
        "ezpublish-legacy-dir": "ezpublish_legacy"
    }
}
```

### eZ Publish 4 case (Netgen variant)
You can also configure the installer to consider the current folder to be the legacy `extension` folder. You can do that by setting `netgen-legacy-install` extra variable in your `composer.json`:

```json
{
    "name": "myvendorname/myproject",
    "description": "My super cool eZ Publish project",
    "license": "GPL-2.0",
    "minimum-stability": "dev",
    "require": {
        "php": ">=5.3.3",
        "netgen/eztags": "~2.0"
    },
    "repositories" : [
        {
             "type": "vcs",
             "url": "https://bitbucket.org/netgen/eztags.git"
        }
    ],
    "extra": {
        "netgen-legacy-install": true
    }
}
```
